first_name: str = input('User name: ')
age: int = int(input('Age: '))

print(f'Hello, \'{first_name}\' you are {age} years old!')
