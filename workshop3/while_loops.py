
# i: int = 1
# while i < 10:
#     # code fragment
#     print(f'Hello {i}')
#     # i = i + 1
#     i += 1

# print(i)

# while True:
#     text = input('Enter text: ')
#     if text == 'stop':
#         break
#     print(text)


text: str = ''

while text.lower() != 'stop':
    text = input('Enter text: ')
    if text.lower() == 'kkk':
        continue

    if text.lower() != 'stop':
        print(text)

   
