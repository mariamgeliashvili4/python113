print('\nFor with 1 argument')
for i in range(10):
    print(i)


print('\nFor with 2 argument')
for i in range(5, 10):
    print(i)


print('\nFor with 3 arguments')
for i in range(5, 10, 2):
    print(i)
