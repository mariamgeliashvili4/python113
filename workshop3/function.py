
# function definition
def greet_user(name: str) -> str:
    # function body / code fragment
    # print('Hello there traveler!')
    return f'Hello there {name}!'


# function call
print(greet_user(input('Enter your name: ')))
text: str = greet_user('James')
print(text)



