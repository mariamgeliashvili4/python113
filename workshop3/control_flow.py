name: str = 'Johnny'
age: int = 17
weather: str = 'Sunny'

if age < 18:
    # code fragment
    print('You are a minor!')
    print('here is your candy!')
else:
    print('You are an adult!')


if name == 'John':
    print('You have a great name!')


if weather.lower() == 'sunny':
    print('I should take my glasses')
elif weather == 'Snowy':
    print('I should wear a warm hat')
elif weather == 'Rainy':
    print('I should take umbrella')
else:
    print('What is happening')
